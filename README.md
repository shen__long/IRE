# [IRE 公开数据](https://gitee.com/arlionn/IRE) 

> [项目主页 - 数据下载](https://gitee.com/arlionn/IRE)

## IRE 介绍

中山大学岭南学院产业与区域经济研究中心（IRE）已公布的数据资料包括：岭南学院地方官员数据库 、中国方言数据、中国各省区市经济增长目标数据、中国地级行政审批中心数据、中国资本主义工商业改造历史数据等。各数据库简要说明和下载链接如下：

### 1. 岭南学院地方官员数据库

- 在王贤彬博士和徐现祥博士努力下，中山大学岭南学院产业与区域经济研究中心收集了1978-2008年间中国省区市党政正职领导人简历。其中，省区市党政正职领导人简历信息包括领导人姓名、在任省份、职位级别、开始年份、终止年份、性别、教育程度等。 
- 引用请标注：岭南学院地方官员数据库（王贤彬、徐现祥，2013）。
- 省长数据下载：[Stata](https://gitee.com/arlionn/IRE/raw/master/Data/governor_information.dta) ；[Excel](https://gitee.com/arlionn/IRE/raw/master/Data/governor_information.xls)
- 省委书记数据下载：[Stata](https://gitee.com/arlionn/IRE/raw/master/Data/secretary_information.dta)；[Excel](https://gitee.com/arlionn/IRE/raw/master/Data/secretary_information.xlsx)

### 2. 省长省委书记异地交流数据

- 省长省委书记异地交流数据提供1978-2005年间省长、省委书记调配交流情况。信息收集自1978—2005 年间全国30个省区市（不包括重庆和港澳台）党委书记以及省长（或市长 、主席）的任命、调动资料。资料来源于《中华人民共和国职官志》以及人民网、新华网等公布的干部资料。
- [引用规则：徐现祥、王贤彬、舒元，《地方官员与经济增长——来自中国省长、省委书记交流的证据》，《经济研究》，2007年第9期，第18-31页。](https://quqi.gblhgk.com/s/51950/2cibW3ZO1YDs2LXh)
- 数据下载：[Stata](https://gitee.com/arlionn/IRE/raw/master/Data/governor_secretary_exchange.dta)；[Excel](https://gitee.com/arlionn/IRE/raw/master/Data/governor_secretary_exchange.xlsx)
      
### 3. 中央到地方交流任职的省长省委书记
- 京官列表提供1978-2005年全国31个省（自治区、直辖市），从中央部委到地方任职的省长省委书记交流信息。期间共发生了49次京官交流，具体数据描述详见《中国地方官员治理的增长绩效》一书。
- 引用规则：徐现祥、王贤彬，2001：《地方官员培养与经济增长》收录到《中国地方官员治理的增长绩效》，科学出版社。
- 数据下载：[Stata](https://gitee.com/arlionn/IRE/raw/master/Data/governor_central_to_local.dta)；[Excel](https://gitee.com/arlionn/IRE/raw/master/Data/governor_central_to_local.xlsx)

### 4. 1986年中国各县方言归属数据
- “1986年中国各县方言归属数据”由中山大学岭南学院徐现祥团队依据《汉语方言大词典》及1986年《中华人民共和国行政区划简册》整理所得。《汉语方言大词典》（许宝华、宫田一郎主编，中华书局，1999年）将汉语方言从粗到细依次分为方言大区、方言区、方言片，按照1986年的行政区划记录了各县方言。本数据完全尊重《大词典》的记录进行整理，并与1986年民政部行政区划及代码进行匹配。1986年中国共有2835个县级行政单位，《大词典》覆盖了其中2615个，约92.2%。对于《大词典》覆盖的县，数据完全尊重大词典进行整理；《大词典》未覆盖的县也包括在内，但其汉语方言归属留空。
- [引用规则：刘毓芸、徐现祥、肖泽凯，《劳动力跨方言流动的倒 U 型模式》，《经济研究》，2015，50(10): 134-146。](https://quqi.gblhgk.com/s/51950/HsyIDRrOSIqbuMXP)
- 数据下载：[Stata](https://gitee.com/arlionn/IRE/raw/master/Data/1986_county_dialect.dta)；[Excel](https://gitee.com/arlionn/IRE/raw/master/Data/1986_county_dialect.xls)

### 5. 中国方言多样性指数
- 肖泽凯根据《汉语方言大词典》中2113个县及以上观测单元所使用的汉语方言，构建了278个地级及以上城市的方言多样性指数。徐现祥、刘毓芸、肖泽凯（2014）首次采用这个指数考察了方言多样性对经济增长的影响。
- [引用出处：徐现祥、刘毓芸、肖泽凯，《方言与经济增长》，《经济学报》，2015年第2期。](https://quqi.gblhgk.com/s/51950/bdz2MxNCFsCIGeS7)
- 数据下载：[Stata](https://gitee.com/arlionn/IRE/raw/master/Data/China_dialect_diversity_index.dta)；[Excel](https://gitee.com/arlionn/IRE/raw/master/Data/China_dialect_diversity_index.xlsx)

### 6. 中国各省区市经济增长目标数据
- 中国经济增长目标数据库由中山大学岭南学院徐现祥教授团队依据各级政府的工作报告文本以及五年规划文本整理所得。这是一项持续的接力工作，梁剑雄、高元骅、李书娟和陈邱惠等先后参与这项工作。
- [引用规则：徐现祥、梁剑雄，《经济增长目标的策略性调整》，《经济研究》，2014年第1期: 27-40。](https://quqi.gblhgk.com/s/51950/JxwJxxFYZ1rgwujY)
- 数据下载：[Stata](https://gitee.com/arlionn/IRE/raw/master/Data/Province_growth_target_2000-2018.dta)；[Excel](https://gitee.com/arlionn/IRE/raw/master/Data/Province_growth_target_2000-2018.xlsx)

### 7. 中国地级行政审批中心数据
- 中国地级行政审批中心数据库由中山大学岭南学院徐现祥教授团队，依据各地行政审批中心官方主页的公示信息整理构建。目前发布的是截至2015年12月，333个地级行政审批中心相关数据，主要包含行政审批中心设立时间、进驻部门数量、进驻事项数量、进驻窗口数量等变量。
- [引用规则：毕青苗、陈希路、徐现祥、李书娟，《行政审批改革与企业进入》，《经济研究》， 2018年第 2 期: 140-155。](https://quqi.gblhgk.com/s/51950/jxRhCvCZYxGIU9Ii)
- 数据下载：[Stata](https://gitee.com/arlionn/IRE/raw/master/Data/Administrative_Approval_Centers.dta)；[Excel](https://gitee.com/arlionn/IRE/raw/master/Data/Administrative_Approval_Centers.xlsx)

### 8. 中国资本主义工商业改造历史数据
- 1949-1956年各省区私营工业总产出数据收集自《中国资本主义工商业的社会主义改造》资料丛书。徐现祥和李郇（2005）采用各省区1949-1956年间私营工业的最大实际总产值作为当今各省区社会基础设施的工具变量，定量分析我国省区经济差距的内生制度根源。
- [引用规则：徐现祥、李郇，《中国省区经济差距的内生制度根源》，《经济学（季刊）》，2005年10月，第81-100页。](https://quqi.gblhgk.com/s/51950/FEFer2130rhbGFpT)
- 数据下载：[Stata](https://gitee.com/arlionn/IRE/raw/master/Data/output.dta)；[Excel](https://gitee.com/arlionn/IRE/raw/master/Data/output.xls)

### 9. 分省区分产业的固定资本存量
- 分省区分产业的固定资本存量数据包含中国各省区三次产业1978-2002年的物质资本存量。采用永续盘存法, 基于《中国国内生产总值核算历史资料:1952—1995》和《中国国内生产总值核算历史资料:1996—2002》 估计得到。
- [引用规则：徐现祥、周吉梅、舒元，《中国省区三次产业物质资本存量估计》，《统计研究》，2007年第5期，第6-13页。](https://quqi.gblhgk.com/s/51950/ZkncgCzTMJmXrysX)
- 数据下载：[Stata](https://gitee.com/arlionn/IRE/raw/master/Data/Province_industrial_capitalstock1978-2002.dta)；[Excel](https://gitee.com/arlionn/IRE/raw/master/Data/Province_industrial_capitalstock1978-2002.xls)

&emsp;

---


&emsp;
 
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)

&emsp;

## 1. 连享会课程

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/lianxh/Stata33)   
> - [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  

&emsp;

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 2. 资源分享

### 视频公开课

- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown](https://gitee.com/arlionn/md) | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)
- [连享会 · Stata 33 讲 - 免费听](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课件](https://gitee.com/arlionn/stata101)
- [直击面板数据模型](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)
- [Stata小白的取经之路](https://gitee.com/arlionn/StataBin)，龙志能，上海财经大学，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) 

### Stata

- [连享会推文](https://www.lianxh.cn) | [直播视频](http://lianxh.duanshu.com)
- **计量专题课程**: [Stata暑期班/寒假班](https://gitee.com/lianxh/text) | [专题课程](https://gitee.com/arlionn/Course)
- Stata专栏：[最新推文](https://www.lianxh.cn) | [知乎](https://www.zhihu.com/people/arlionn/) | [CSDN](https://blog.csdn.net/arlionn)
- Books and Journal: [计量Books](https://quqi.gblhgk.com/s/880197/hmpmu2ylAcvHnXwY) | [SJ-PDF](https://quqi.gblhgk.com/s/880197/eipgoUi6Gd1FDZRu) | [Stata Journal-在线浏览](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
- Stata Guys：[Ben Jann](http://www.soz.unibe.ch/about_us/personen/prof_dr_jann_ben/index_eng.html) 

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) 
- [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) 
- [徐现祥教授-IRE-官员交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)
- [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)

### Papers - 学术论文复现
- [论文重现网站](https://www.lianxh.cn/news/e87e5976686d5.html)
- [Google学术](https://ac.scmor.com/) | [统一入口：虫部落学术搜索](http://scholar.chongbuluo.com/) | [微软学术](https://academic.microsoft.com/home)
- [iData - 期刊论文下载](https://www.cn-ki.net/)
- [ CNKI ](http://scholar.cnki.net/) | [百度学术](http://xueshu.baidu.com/) | [Google学术](https://scholar.glgoo.org/) | [Sci-hub ](http://www.sci-hub.cc/), [2](http://sci-hub.ac/), [3](http://sci-hub.bz/), [4](http://sci-hub.ac/)
- Stata论文重现:  [Harvard dataverse][harvd] | [JFE][jfe]  | [github][git1] | [Yahoo-github][yahoogit]
- 学者主页(提供了诸多论文的原始数据和 dofiles)：[Angrist][Ang1] || [Daron Acemoglu][acem]  || [Ross Levine][ross] || [Esther Duflo][Duflo] || [Imbens](https://scholar.harvard.edu/imbens/software)  ||  [Raj Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。

&emsp; 

> &#x26F3;  **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;

