


### 请 陈邱惠 参阅

----
> `2019/5/28 9:10`

这几天我琢磨了一下数据的发布方式：
1. 在 码云 上建立项目主页，以网页形式提供数据和说明文档的下载链接，以便读者可以自行选择下载；这些内容在【readme.md】文档中撰写；
2. 在 GitHub 上放置一个备份 (主要是 **.dta** 文件)，使用 **cnuse** 命令进行下载。
   - 在此之前，我要先发布 cnuse 命令；
   - 你在我建立的 GitHub 主页上上传数据文件；
   - 写一个推文，对公开数据项目进行介绍，并说明两种下载方式的具体操作步骤。

---
> `2019/5/17 9:41`

目前数据已经传完，下一步可以开始写程序了。  
1. 相关说明参见 【prog】文件夹 
2. 我们程序命名为 **cnuse.ado**
3. 程序编写思路    
  - 使用 `ssc intall bcuse, replace` 下载外部命令，会自动存放在 **...\stata15\ado\plus\b** 文件夹下
  - 请在 Dofile 编辑器中打开 `bcuse.ado` 文件，另存为 **cnuse**，文件类型选择 **.ado**  
  - 根据我们存放数据的路径修改 **cnuse.ado** 文件中的相关语句，并测试是否可行  

&emsp;

---
> `2019/5/11`
- 点击 [[连享会推文列表]](https://www.jianshu.com/p/de82fdc2c18a), 查看 **Markdown** 栏目下的相关推文，了解 Markdown 的语法格式； 
- 有关码云操作等，可以参见 [「码云使用指南」](https://gitee.com/Stata002/StataSX2018/wikis/%E7%A0%81%E4%BA%91%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97?sort_id=937861) 页面中的相关建议和要求。你不需要学习太复杂的内容，只需要了解如何注册账号，上传文件即可。
- 你处理好的文件放到 [[IRE - Data]](https://gitee.com/arlionn/IRE/tree/master/Data) 文件夹根目录下即可，我可以去那里查看，这样就不必要每次频繁地传递文件了。



&emsp;








