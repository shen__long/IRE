prog drop _all

*cnuse  20190517
prog cnuse
	version 14
	syntax anything [, CLEAR noDesc]
	if regexm("`anything'", ".+.dta$") {
		capt copy "https://gitee.com/arlionn/IRE/raw/master/Data/`anything'" .
		use "`anything'",clear
		if _rc != 0 {
			di as err _n "Error: cannot copy `anything' to current working directory."
			exit 
		}
	}
	if _rc != 0 {
 		if _rc == 4 {
 			di as err _n "Error: data in memory would be lost. Use  cnuse `anything', clear  to discard changes."
 		} 
 		else if _rc == 610 {
 			di as err _n "Error: IRE datafile `anything' must be accessed from Stata 14.x." _n  "It cannot be read by earlier Stata versions."
 		}
 		else {
		di as err _n "Error: IRE datafile `anything' does not exist. Contents of memory not altered."
		}
		exit
	}
	if "`desc'" != "nodesc" { 
		describe
	}
	capt tsset
	if _rc==0 {
		tsset
	}
end
