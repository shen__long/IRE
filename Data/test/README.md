
## 数据调用和下载测试文件

我们放在网上的数据可以使用如下命令存入 Stata (注意：`copy` 命令后的文件名一定要加双引号)：

```Stata
copy "https://gitee.com/arlionn/IRE/raw/master/Data/test/autox.dta" . // 直接保存
copy "https://gitee.com/arlionn/IRE/raw/master/Data/test/autox.dta" auto4.dta // 另存
```

编写 **.ado** 文件时，可以参考外部命令 `bcuse.ado` 的编写思路，主要是各种 bug 的应对方法。
